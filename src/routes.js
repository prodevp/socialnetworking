import React, { Component } from "react";
import { StackNavigator } from "react-navigation";
import { Home ,Login} from "./Components";
const routeConfiguration = {
  Home: { screen: Home },
  Login: { screen: Login }
};

const stackNavigatorConfiguration = {
  initialRouteName: "Home"
};

stackNavigatorConfiguration.navigationOptions = ({ navigation }) => {
  return {
    header: null
  };
};

export const Routes = StackNavigator(routeConfiguration, stackNavigatorConfiguration);
