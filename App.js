import React, { Component } from 'react';
import {Routes} from './src/routes';
import {Root} from "native-base";
export default class App extends Component {
  render() {
    return (
      <Root>
      <Routes/>
      </Root>
    );
  }
}